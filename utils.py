import json 
from copy import deepcopy

subject = []
teacher = [] 
section = []
teach_sub = {}
section_tt = {}
teach_work_style = {}
teacher_tt = {}
teach_section = {}
sub_section_teacher= {}

def import_teacher_names(teacher , teach_sub , teach_section , teach_work_style) : 
    '''
        Function to import teacher from teacher.json
        need data trunkation
        params : teacher , teach_sub , teach_section
        return : none
    '''
    f = open('teacher.json')
    data = json.load(f)
    for k,v in data.items() : 
        teacher.append(k)
        teach_sub[k] = v["subjects"]
        teach_section[k] = v['sections']
        teach_work_style[k] = v['prefered_work_style']

def import_section_names(section) :
    '''
        Function to import section from section.json 
        need data trunkation
        params : Section
        return : none
    '''
    f= open('section.json')
    data = json.load(f)
    assert isinstance(data["section"] , list) , "Will Not work , data provided for subject wrong"
    assert False if len(data["section"]) != 5 else True  , "Will Not work , data provided for subject wrong"
    section.extend( data["section"] )  

def import_subject_names(subject) : 
    '''
        Function to import subject from subject.json
        need to do data truncation : if subject isnt a list assert -done
        params : subject
        return : none
    '''
    f= open('subject.json')
    data = json.load(f)
    assert isinstance(data["subject"] , list) , "Will Not work , data provided for subject wrong"
    assert False if len(data["subject"]) != 5 else True  , "Will Not work , data provided for subject wrong"
    subject.extend( data["subject"] ) 
 
def help_assign_teacher_subject(teach_sect_dict , subject_list , section  ) :   
    '''
        Helper function that assigns teacher to subjects 
    ''' 

    for sec , it_teach in teach_sect_dict.items() :
        for teach , sub_list in it_teach.items():
            teach_sect_dict[sec][teach] = { sub_list[0] : 4} 
    pass
    
def help_assign_teach_sect(section , teach_sub , teach_section  ):
    # taps : Teacher available per section 
    '''
        Function : Assigning all the teachers for the subject that is given 
        parameters :section_list , containing all the sections 
                    teach_sub : for the teachers and their respective subjects 
                    teach_section : for the teacher and their respective subjects 
        return : Dictonary returning all the data of teach _ section _ subject
        Helper function for assigning teacher to section
    '''
    taps = {}
    for it_sec in section : 
        for k , v in teach_section.items() :
            if it_sec in v :
                if it_sec in taps : 
                    taps[it_sec][k] = teach_sub[k]
                else :
                    taps[it_sec] = {k:teach_sub[k]}
    return taps 
    

def assign_teacher_subject (subject , section , teach_sub , teach_section ) :
    # Here adding the section wise sorting teachers
    '''
        Function : assigning teacher to subject and section (for now arbitary subject [a] being taken need to update that ) 
        Params   : Subject : list
                   section : list
                   teach_sub : dictionary of teacher subject
                   teach_section : teacher to section mapping 
        Return   : teach_avail_per_sec 
    '''
    teach_avail_per_sec = {}    
    teach_avail_per_sec = help_assign_teach_sect(section , teach_sub = teach_sub , teach_section = teach_section )
    
    
    help_assign_teacher_subject(  section = section , subject_list = subject , teach_sect_dict = teach_avail_per_sec  )
    
    return teach_avail_per_sec

def helper_gen_timetable_matrix():
    '''
        Simple helper function that outputs empty timetable
    '''
    tt = [ ]
    list = [ 0 , 0 ,0 , 0 ,0 ]
    for i in range(0 , 4) :
        tt.append(deepcopy(list))
    return tt 

def rotate_teachers( teachers , sub_section_teach , sec ):
    '''
        Function to rotate the teacher list so that we can have no clash 
        params : teachers list
                 sub section teach - subject to section to teacher
                 sec - section name 
    '''
    sec_rotate_list = {
        'A' : 0,
        'B' : 1,
        'C' : 2, 
        'D' : 0,
        'E' : 2
    }
    teachers = list(teachers)
    list_len = len(teachers)
    for i in range(0 , sec_rotate_list[sec]):
        teachers = [teachers[list_len-1]] + teachers[0:list_len-1]
    return teachers

def transpose(time_tt_temp):
    '''
        Helper function : To transpose the matrix 
    '''
    time_tt = [[time_tt_temp[j][i] for j in range(len(time_tt_temp))] for i in range(len(time_tt_temp[0]))]
    return time_tt

def gen_timetable_section (sub_section_teacher , teacher , section , teach_work_style) : 
    '''
        Cyclic function to generate the timetable for section wise 
        parmas :    sub_section_teacher
                    teacher
                    section 
                    teach_Work_style
        return : None
    '''
    section_tt = {} 
    sub_c= 0 
    for sec, it_teach_sub in sub_section_teacher.items() : 
        
        teachers_list_per_class = rotate_teachers(it_teach_sub.keys() , sub_section_teacher , sec)
        
        section_tt[sec] = {"timetable" : [] , "isGen" : False}
        
        count = 0 
        temp_tt = helper_gen_timetable_matrix()
        tc= 0 
        for i in range(0 ,4):
            for j in range(0 , 5) :
                teacher_val =  teachers_list_per_class[0]
                if teach_work_style[teacher_val] ==1 : 
                    if temp_tt[i][j]  not in teacher :
                        temp_tt[i][j] = teacher_val
                        tc+=1
                elif teach_work_style[teacher_val] ==2:
                    if temp_tt[i][j]  not in teacher :
                        temp_tt[i][j] = teacher_val
                        temp_tt[(i+1)%4][j] = teacher_val
                        tc+=2
                if tc % 4 == 0 :
                    count +=1
                    teachers_list_per_class.pop(0)
        sub_c+=1
        section_tt[sec]['timetable'] = transpose(temp_tt)
            
        section_tt[sec]["isGen"] = True
        sub_section_teacher[sec]['timetable'] = section_tt[sec]['timetable']
        sub_section_teacher[sec]['isGen'] = section_tt[sec]['isGen']

def display_section_tt(sub_section_teacher , section_tt):
    '''
        Helper function to display the section and also return a section : timetable 

    '''
    for section , it_sub_sec in sub_section_teacher.items():
        print("---------" , section , " : TT -----------")
        for attr , it_attr in it_sub_sec.items():
            if attr != 'timetable' :
                print(attr , it_attr)
        tt= it_sub_sec['timetable']
        section_tt[section] = tt
        for i in tt:
            for j in i:
                print(j , end=" ")
            print()
        print()
    print()

def get_section_tt(sub_section_teacher , section_tt):
    '''
        Getter function to intialize the timetable generated for sections 
    '''
    for section , it_sub_sec in sub_section_teacher.items():
        tt= it_sub_sec['timetable']
        section_tt[section] = tt
    return section_tt


def display_teach_tt(teacher_tt):
    '''
        Helper function to display
    '''
    for teacher , tt in teacher_tt.items() :
        print(teacher , "------------------")
        for i in tt:
            for j in i :
                print(j, end=" ")
            print()
    pass

def generate_teacher_tt(teacher_list , section_tt ):
    '''
        Generator function for time table 
        params : teachers 
                section wise time table
        return : teacher wise time table
    '''
    teacher_tt = {}
    for teacher in teacher_list : 
        temp_teacher_tt = transpose(helper_gen_timetable_matrix())
        for section , tt in  section_tt.items() : 
            for i in range(0 , 5):
                for j in range(0, 4) :
                    if teacher == tt[i][j] :
                        temp_teacher_tt[i][j] = section
        teacher_tt[teacher] = temp_teacher_tt
    
    return teacher_tt
       

if __name__ == "__main__" : 

    import_teacher_names(teacher=teacher , teach_sub=teach_sub , teach_section=teach_section , teach_work_style
    = teach_work_style)
    import_subject_names(subject=subject)
    import_section_names(section)
    sub_section_teacher = assign_teacher_subject(subject , section , teach_sub , teach_section  )
    gen_timetable_section(sub_section_teacher , teacher , section , teach_work_style)
    section_tt = get_section_tt(sub_section_teacher , section_tt )
    print("Here")
    display_section_tt(sub_section_teacher , section_tt)
    teacher_tt = generate_teacher_tt(teacher , section_tt  )
    display_teach_tt(teacher_tt)
    