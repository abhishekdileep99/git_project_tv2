from utils import * 
from input_truncator import *
subject = []
teacher = [] 
section = []
teach_sub = {}
section_tt = {}
teach_work_style = {}
teacher_tt = {}
teach_section = {}
sub_section_teacher= {}

check_data()

import_teacher_names(teacher=teacher , teach_sub=teach_sub , teach_section=teach_section , teach_work_style
    = teach_work_style)
import_subject_names(subject=subject)
import_section_names(section)
sub_section_teacher = assign_teacher_subject(subject , section , teach_sub , teach_section  )
gen_timetable_section(sub_section_teacher , teacher , section , teach_work_style)
section_tt = get_section_tt(sub_section_teacher , section_tt )
display_section_tt(sub_section_teacher , section_tt)
teacher_tt = generate_teacher_tt(teacher , section_tt  )
display_teach_tt(teacher_tt)

