import json 
from utils import * 
teacher = {}
teacher_sub = {}

def check_data( ) : 
    subject = []
    teacher = [] 
    section = []
    teach_sub = {}
    section_tt = {} 
    teach_work_style = {}
    teacher_tt = {}
    teach_section = {}
    sub_section_teacher= {}
    import_teacher_names(teacher=teacher , teach_sub=teach_sub , teach_section=teach_section , teach_work_style
    = teach_work_style)
    import_subject_names(subject=subject)
    import_section_names(section)
    sub_section_teacher = assign_teacher_subject(subject , section , teach_sub , teach_section  )

    assert len(subject) == 5  , "error : subjects not enough "
    assert len(teacher) == 10 , "error , teachers not enough "
    for k , v in teach_sub.items(): 
        assert len(v) < 3 , "too many subjects for teacher "
    for k , v in teach_section.items():
        assert len(v) < 4 , "too many sections for teacher"
    for k,v in teach_work_style.items():
        class_teach = len(teach_section[k])
        if v > 1 :
            assert class_teach == 2 , "error teacher has more than 2 section for 2 hour block " #If teacher has 3 sec , he will have 12 hours to fill and that will create a conflict in his sub for 2 hour blocks 
    